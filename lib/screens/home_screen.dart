import 'package:flutter/material.dart';
import 'package:peliculas/providers/movies_provider.dart';
import 'package:peliculas/search/search_delegate.dart';
import 'package:peliculas/widgets/widgets.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final moviesProvider = Provider.of<MoviesProvider>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Peliculas en cines'),
        elevation: 0,
        centerTitle: true,
        actions: [
          IconButton(
              icon: const Icon(Icons.search_rounded, color: Colors.white),
              onPressed: () => showSearch(
                  context: context, delegate: MovieSearchDelegate())),
        ],
      ),
      body: SingleChildScrollView(
          child: Column(children: [
        CardSwipper(movies: moviesProvider.onDisplayMovies),
        MovieSlider(
            movies: moviesProvider.popularMovies,
            title: 'Pompulares',
            onNextPage: () => moviesProvider.getPopularMovies()),
      ])),
    );
  }
}
